package ru.bakhtiyarov.tm.api.service;

import ru.bakhtiyarov.tm.model.Project;
import ru.bakhtiyarov.tm.model.Task;

import java.util.List;

public interface IProjectService {

    void create(String name);

    void create(String name, String description);

    void add(Project task);

    void remove(Project task);

    List<Project> findAll();

    void clear();

}
