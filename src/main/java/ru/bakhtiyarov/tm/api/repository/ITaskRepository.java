package ru.bakhtiyarov.tm.api.repository;

import ru.bakhtiyarov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(Task project);

    void remove(Task project);

    List<Task> findAll();

    void clear();

}
