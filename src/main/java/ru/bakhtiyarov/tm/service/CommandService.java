package ru.bakhtiyarov.tm.service;

import ru.bakhtiyarov.tm.api.repository.ICommandRepository;
import ru.bakhtiyarov.tm.api.service.ICommandService;
import ru.bakhtiyarov.tm.model.Command;

public class CommandService implements ICommandService {

    private ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

    @Override
    public String[] getCommands() {
        return commandRepository.getCommands();
    }

    @Override
    public String[] getArgs() {
        return commandRepository.getArgs();
    }

}
