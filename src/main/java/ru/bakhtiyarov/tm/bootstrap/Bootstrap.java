package ru.bakhtiyarov.tm.bootstrap;

import ru.bakhtiyarov.tm.api.controller.ICommandController;
import ru.bakhtiyarov.tm.api.controller.IProjectController;
import ru.bakhtiyarov.tm.api.controller.ITaskController;
import ru.bakhtiyarov.tm.api.repository.ICommandRepository;
import ru.bakhtiyarov.tm.api.repository.IProjectRepository;
import ru.bakhtiyarov.tm.api.repository.ITaskRepository;
import ru.bakhtiyarov.tm.api.service.ICommandService;
import ru.bakhtiyarov.tm.api.service.IProjectService;
import ru.bakhtiyarov.tm.api.service.ITaskService;
import ru.bakhtiyarov.tm.constant.ArgumentConst;
import ru.bakhtiyarov.tm.constant.TerminalConst;
import ru.bakhtiyarov.tm.controller.CommandController;
import ru.bakhtiyarov.tm.controller.ProjectController;
import ru.bakhtiyarov.tm.controller.TaskController;
import ru.bakhtiyarov.tm.repository.CommandRepository;
import ru.bakhtiyarov.tm.repository.ProjectRepository;
import ru.bakhtiyarov.tm.repository.TaskRepository;
import ru.bakhtiyarov.tm.service.CommandService;
import ru.bakhtiyarov.tm.service.ProjectService;
import ru.bakhtiyarov.tm.service.TaskService;
import ru.bakhtiyarov.tm.util.TerminalUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);


    public void run(final String... args) {
        commandController.displayWelcome();
        if (parseArgs(args)) System.exit(0);
        while (true) parseCommand(TerminalUtil.nextLine());

    }

    private boolean parseArgs(final String... args) {
        if (args == null || args.length < 1) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    private void parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.ABOUT:
                commandController.displayAbout();
                break;
            case ArgumentConst.HELP:
                commandController.displayHelp();
                break;
            case ArgumentConst.VERSION:
                commandController.displayVersion();
                break;
            case ArgumentConst.INFO:
                commandController.displayInfo();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.displayArguments();
                break;
            case ArgumentConst.COMMANDS:
                commandController.displayCommands();
                break;
            default:
                break;
        }
    }

    private void parseCommand(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case TerminalConst.ABOUT:
                commandController.displayAbout();
                break;
            case TerminalConst.HELP:
                commandController.displayHelp();
                break;
            case TerminalConst.VERSION:
                commandController.displayVersion();
                break;
            case TerminalConst.INFO:
                commandController.displayInfo();
                break;
            case TerminalConst.EXIT:
                commandController.exit();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.displayArguments();
                break;
            case TerminalConst.COMMANDS:
                commandController.displayCommands();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showTasks();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showProject();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProject();
                break;
            default:
                break;
        }
    }

}
