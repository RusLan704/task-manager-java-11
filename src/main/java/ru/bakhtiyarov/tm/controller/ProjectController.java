package ru.bakhtiyarov.tm.controller;

import ru.bakhtiyarov.tm.api.controller.IProjectController;
import ru.bakhtiyarov.tm.api.service.IProjectService;
import ru.bakhtiyarov.tm.model.Project;
import ru.bakhtiyarov.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements IProjectController {

    private IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showProject() {
        System.out.println("[LIST PROJECT]");
        final List<Project> projects = projectService.findAll();
        for (Project project : projects) System.out.println(project);
        System.out.println("[OK]");
    }

    @Override
    public void clearProject() {
        System.out.println("[CLEAR PROJECT]");
        projectService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        projectService.create(name, description);
        System.out.println("[OK]");
    }

}
